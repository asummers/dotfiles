;;; package --- Summary: init file

;;; Commentary:


;;; Code:

;;; -----package-----
(require 'package)
(setq package-enable-at-startup nil)

(add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/"))
(add-to-list 'package-archives '("marmalade" . "https://marmalade-repo.org/packages/"))
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/") t)

(package-initialize)

(when (not package-archive-contents) (package-refresh-contents))

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(add-to-list 'load-path "~/.emacs.d/plugins/")

;;; -----use-package-----
(eval-when-compile
  (require 'use-package))


;;; -----OS X-----

(setq mac-command-modifier 'meta)
(setq mac-option-modifier 'super)
(setq ns-function-modifier 'hyper)


;;; -----diminish-----
(use-package diminish
  :ensure t
  :defer t
  :init
  (eval-after-load "yasnippet"
    '(diminish 'yas-minor-mode "Y"))
  (eval-after-load "hilight-symbol"
    '(diminish 'global-hilight-symbol-mode "Y")))


;;; -----sane-defaults-----
(use-package cl :defer t)
(use-package sane-defaults
  :load-path "~/.emacs.d/plugins/")


;;; -----guru-mode-----
(use-package guru-mode
  :ensure t
  :defer t
  :diminish guru-mode
  :init
  (guru-global-mode))

;;; -----theme-----
(setq custom-file "~/.emacs.d/emacs-custom.el")
(load custom-file)
(load-theme 'base16-eighties-dark t)
(set-frame-parameter (selected-frame) 'alpha '(93 93))
(add-to-list 'default-frame-alist '(alpha 93 93))


;;; -----rainbow-mode-----
(use-package rainbow-mode
  :ensure t
  :defer t
  :init
  (rainbow-mode t))


;;; -----rainbow-identifiers-----
(use-package rainbow-identifiers
  :ensure t
  :defer t
  :init
  (progn
    (add-hook 'prog-mode-hook 'rainbow-identifiers-mode)
    (setq rainbow-identifiers-choose-face-function 'rainbow-identifiers-cie-l*a*b*-choose-face
          rainbow-identifiers-cie-l*a*b*-lightness 85
          rainbow-identifiers-cie-l*a*b*-saturation 38
          rainbow-identifiers-cie-l*a*b*-color-count 50
          ;; override theme faces
          rainbow-identifiers-faces-to-override '(highlight-quoted-symbol
                                                  font-lock-variable-name-face
                                                  font-lock-function-name-face
                                                  font-lock-type-face
                                                  js2-function-param
                                                  js2-external-variable
                                                  js2-instance-member
                                                  js2-private-function-call))))


;;; -----volatile-highlights
(use-package volatile-highlights
  :ensure t
  :disabled t
  :defer t
  :init
  (volatile-highlights-mode t))

;;; ----move text -----
(use-package move-text
  :ensure t
  :defer t
  :bind
  (("M-<up>" . move-text-up)
   ("M-<down>" . move-text-down)))


;;; -----tern-----
(use-package tern
  :ensure t
  :defer t
  :diminish tern-mode
  :bind
  ("<f2>" . tern-find-definition)
  :init
  (add-hook 'js2-mode-hook (lambda () (tern-mode t))))


;;; -----highlight-symbol-----
(use-package highlight-symbol
  :ensure t
  :defer t
  :diminish highlight-symbol-mode
  :config
  (add-hook 'prog-mode-hook #'highlight-symbol-mode)
  (add-hook 'prog-mode-hook #'highlight-symbol-nav-mode)
  (setq highlight-symbol-on-navigation-p t)
  (setq highlight-symbol-idle-delay 0.25)
  ; (setq highlight-symbol-foreground-color "white")
  (setq next-line-add-newlines nil))

(use-package hl-line-mode
  :defer t
  :init
  (global-hl-line-mode 1)
  :config
  (set-face-background hl-line-face "gray10"))


;;; -----nlinum-----
(use-package nlinum
  :ensure t
  :defer t
  :diminish nlimum
  :init
  (global-nlinum-mode t)

  ; Space after number
  (setq nlinum-format "%d ")

  (defun my/nlinum-mode-hook ()
      (unless (boundp 'nlinum--width)
        (setq nlinum--width
              (length (number-to-string
                       (count-lines (point-min) (point-max)))))))

  (add-hook 'nlinum-mode-hook 'my/nlinum-mode-hook))


;;; -----org-mode-----
(use-package org-mode
  :defer t
  :bind
  (("C-c l" . org-store-link)
   ("C-c a" . org-agenda))
  :mode ("\\.org$" . org-mode)
  :init
  (setq org-src-fontify-natively t)
  (setq org-return-follows-link t)
  (setq org-log-done 'time)
  (setq org-use-fast-todo-selection t)
  (setq org-todo-keywords
      '((sequence "TODO(t)" "STARTED(s)" "WANT(w)"
                  "|" "DONE(d)" "CANCELLED(c)"))))


;;; -----avy-----
(use-package avy
  :ensure t
  :defer t
  :bind (("M-s" . avy-goto-word-1)))


;; ;;; -----shell-----
(use-package xterm-color
  :defer t
  :disabled t
  :init
  (add-hook 'shell-mode-hook 'ansi-color-for-comint-mode-on)
  (add-to-list 'comint-output-filter-functions 'ansi-color-process-output)
  (defun init-xterm-color ()
    (progn
      ;; Comint and Shell
      (add-hook 'comint-preoutput-filter-functions 'xterm-color-filter)
      (setq comint-output-filter-functions (remove 'ansi-color-process-output comint-output-filter-functions))
      (setq font-lock-unfontify-region-function 'xterm-color-unfontify-region)
      (with-eval-after-load 'esh-mode
        (add-hook 'eshell-mode-hook (lambda () (setq xterm-color-preserve-properties t)))
        (add-hook 'eshell-preoutput-filter-functions 'xterm-color-filter)
        (setq eshell-output-filter-functions (remove 'eshell-handle-ansi-color eshell-output-filter-functions)))))

  (init-xterm-color))


(use-package multi-shell
  :defer t
  :disabled t
  :bind
  (("<f1>" . multi-shell-new)
   (:map comint-mode-map
         ("C-c c" . comint-clear-buffer)))
  :init
  (setq term-term-name "xterm-256color")
  (setq-default comint-input-ignoredups t)
  (setq comint-scroll-show-maximum-output t)
  (setq comint-scroll-to-bottom-on-input t)
  (setq comint-input-ring-size 10000)
  (setq comint-buffer-maximum-size 10000)

  (defvar comint-history-dir (locate-user-emacs-file "comint-history"))
  (unless (file-exists-p comint-history-dir)
    (make-directory comint-history-dir))

  (defadvice comint-output-filter (after output-readonly activate)
    "Set last process output read-only."
    (add-text-properties comint-last-output-start (line-end-position 0)
                         '(read-only ""
                                     rear-nonsticky (inhibit-line-move-field-capture))))

  (add-hook 'comint-output-filter-functions 'comint-strip-ctrl-m)
  (add-hook 'comint-output-filter-functions 'comint-watch-for-password-prompt)

  (setq explicit-shell-file-name "/usr/local/bin/zsh")
  (setq multi-shell-command "/usr/local/bin/zsh")

  (defun comint-clear-buffer ()
    (interactive)
    (let ((comint-buffer-maximum-size 0))
      (comint-truncate-buffer)))

  (defun turn-on-comint-history ()
    (let ((process (get-buffer-process (current-buffer))))
      (when process
        (setq comint-input-ring-file-name "~/.zsh_history")
        (comint-read-input-ring t)
        (add-hook 'kill-buffer-hook 'comint-write-input-ring t t)
        (set-process-sentinel process
                              #'comint-write-history-on-exit))))

  (add-hook 'comint-mode-hook 'turn-on-comint-history)

  ; persistent comint history
  (defun comint-write-history-on-exit (process event)
    (comint-write-input-ring)
    (let ((buf (process-buffer process)))
      (when (buffer-live-p buf)
        (with-current-buffer buf
          (insert (format "\nProcess %s %s" process event))))))

  ;(add-hook 'kill-buffer-hook 'comint-write-input-ring)

  (defun mapc-buffers (fn)
    (mapc (lambda (buffer)
            (with-current-buffer buffer
              (funcall fn)))
          (buffer-list)))

  (defun comint-write-input-ring-all-buffers ()
    (mapc-buffers 'comint-write-input-ring))

  (setq comint-prompt-read-only t)
  (add-hook 'kill-emacs-hook 'comint-write-input-ring-all-buffers))


;;; -----helm-----
(use-package helm
  :ensure t
  :defer t
  :diminish helm-mode
  :bind
  (("C-c h" . helm-command-prefix)
   ("C-x C-f" . helm-find-files)
   ("C-c o" . helm-occur)
   ("C-x b" . helm-mini)
   ("M-x" . helm-M-x)
   ("M-y" . helm-show-kill-ring))
  :config
  (helm-mode 1)

  (global-unset-key (kbd "C-x c"))
  (define-key helm-map (kbd "<tab>") 'helm-execute-persistent-action) ; rebind TAB to run persistent action
  (define-key helm-map (kbd "C-i") 'helm-execute-persistent-action) ; make TAB work in terminal
  (define-key helm-map (kbd "C-z")  'helm-select-action) ; list actions using C-z

  (helm-autoresize-mode t)

  (setq helm-split-window-in-side-p nil) ; t to open helm buffer inside current window, not occupy whole other window)
  (setq helm-move-to-line-cycle-in-source t) ; move to end or beginning of source when reaching top or bottom of source.
  (setq helm-ff-search-library-in-sexp t) ; search for library in `require' and `declare-function' sexp.
  (setq helm-scroll-amount 8) ; scroll 8 lines other window using M-<next>/M-<prior>
  (setq helm-ff-file-name-history-use-recentf t)

  (setq helm-M-x-fuzzy-match t)
  (setq helm-ag-fuzzy-match t)
  (setq helm-apropos-fuzzy-match t)
  (setq helm-imenu-fuzzy-match t)
  (setq helm-locate-fuzzy-match t)
  (setq helm-recentf-fuzzy-match t)
  (setq helm-semantic-fuzzy-match t)
  (setq-default helm-input-idle-delay 0.01)

  (set-face-attribute 'helm-selection nil
                      :background "grey"
                      :foreground "black")

  (defun helm-hide-minibuffer-maybe ()
  (when (with-helm-buffer helm-echo-input-in-header-line)
    (let ((ov (make-overlay (point-min) (point-max) nil nil t)))
      (overlay-put ov 'window (selected-window))
      (overlay-put ov 'face (let ((bg-color (face-background 'default nil)))
                              `(:background ,bg-color :foreground ,bg-color)))
      (setq-local cursor-type nil))))

  (add-hook 'helm-minibuffer-set-up-hook 'helm-hide-minibuffer-maybe)

  (use-package helm-ag
    :ensure t
    :defer t
    :bind
    (("C-c C-f" . helm-do-ag-buffers)
     ("C-c C-F" . helm-do-ag-buffers))
    :config
    (setq helm-buffers-fuzzy-matching t))

  (use-package helm-spotify
    :defer t
    :load-path "~/.emacs.d/plugins/")

  (use-package helm-flx
    :defer t
    :init
    (helm-flx-mode t))

  (use-package swiper-helm
    :bind
    (("C-s" . swiper-helm)
     ("C-r" . swiper-helm))
    :defer t
    :init
    (defun my/swiper-recenter (&rest args)
      "recenter display after swiper"
      (recenter))

    (advice-add 'swiper-helm :after #'my/swiper-recenter))

  (use-package helm-dash
    :ensure t
    :defer t
    :config
    (defun my/open-with-shell-command (url)
      (shell-command (format "/Applications/Google\\ Chrome.app/Contents/MacOS/Google\\ Chrome \"%s\"" url))
      (shell-command "open -a Google\\ Chrome"))

    (setq helm-dash-browser-func 'my/open-with-shell-command))

  (when (executable-find "curl")
    (setq helm-google-suggest-use-curl-p t)))


(use-package projectile
  :ensure t
  :defer t
  :diminish projectile-mode
  :init
  (projectile-global-mode)
  (setq projectile-completion-system 'helm)
  (helm-projectile-on)
  (setq projectile-enable-caching t)
  (setq projectile-switch-project-action 'helm-projectile))


;;; -----shrink-whitespace-----
(use-package shrink-whitespace
  :defer t
  :bind
  ("s-SPC" . shrink-whitespace))


;;; -----sql-mode-----
;; (use-package sql-mode
;;   :defer t
;;   :init
;;   (custom-set-variables
;;    '(sql-postgres-options (quote ("-P" "pager=off" "-w")))))
  ;; (custom-set-variables
   ;; '(sql-postgres-login-prompts (quote )


;;; -----hippie-expand-----
(use-package hippie-exp
  :ensure t
  :defer t
  :bind
  ("M-/" . hippie-expand)
  :config
  (setq hippie-expand-try-functions-list '(try-expand-dabbrev
                                           try-expand-dabbrev-all-buffers
                                           try-expand-dabbrev-from-kill
                                           try-complete-file-name-partially
                                           try-complete-file-name
                                           try-expand-all-abbrevs
                                           try-expand-list
                                           try-expand-line
                                           try-complete-lisp-symbol-partially
                                           try-complete-lisp-symbol)))


;;; -----flycheck-----
(use-package flycheck
  :ensure t
  :defer t
  :diminish flycheck-mode
  :init
  (add-hook 'after-init-hook #'global-flycheck-mode)
  (setq flycheck-display-errors-function #'flycheck-display-error-messages-unless-error-list)
  (setq flycheck-check-syntax-automatically '(save idle-change mode-enabled)
        flycheck-idle-change-delay 0.8)
  :config
  ;; (flycheck-define-checker javascript-flow
  ;;   "A JavaScript syntax and style checker using Flow.
  ;;   See URL `http://flowtype.org/'."
  ;;   :command ("flow" "check" "--old-output-format" source-original)
  ;;   :error-patterns
  ;;   ((error line-start
  ;;           (file-name)
  ;;           ":"
  ;;           line
  ;;           ":"
  ;;           (minimal-match (one-or-more not-newline))
  ;;           ": "
  ;;           (message (minimal-match (and (one-or-more anything) "\n")))
  ;;           line-end))
  ;;   :modes (js-mode js2-mode js3-mode))

  (setq-default flycheck-disabled-checkers
                (append flycheck-disabled-checkers
                        '(javascript-jshint
                          javascript-gjshint
                          javascript-jscs
                          javascript-standard)))

  (add-to-list 'flycheck-checkers 'javascript-flow t)


  (define-derived-mode flow-mode typescript-mode "Flow"
    "JavaScript with Flow type checking")
  (define-key flow-mode-map (kbd ":") nil)
  (add-to-list 'auto-mode-alist '("\\.jsx$" . flow-mode))

  (require 'f)
  (require 'json)
  (defun flycheck-parse-flow (output checker buffer)
    (let ((json-array-type 'list))
      (let ((o (json-read-from-string output)))
        (mapcar #'(lambda (errp)
                    (let ((err (cadr (assoc 'message errp))))
                      (flycheck-error-new
                       :line (cdr (assoc 'line err))
                       :column (cdr (assoc 'start err))
                       :level 'error
                       :message (cdr (assoc 'descr err))
                       :filename (f-relative
                                  (cdr (assoc 'path err))
                                  (f-dirname (file-truename
                                              (buffer-file-name))))
                       :buffer buffer
                       :checker checker)))
                (cdr (assoc 'errors o))))))

  (flycheck-define-checker javascript-flow
    "Static type checking using Flow."
    :command ("flow" "--json" source-original)
    :error-parser flycheck-parse-flow
    :modes flow-mode)
  (add-to-list 'flycheck-checkers 'javascript-flow))



;;; -----magit-----
(use-package magit
  :ensure t
  :defer t
  :bind
  (("C-x g" . magit-status))
   ;("q" . magit-quit-session)
  :config
  (defadvice magit-status (around magit-fullscreen activate)
    (window-configuration-to-register :magit-fullscreen)
    ad-do-it
    (delete-other-windows))

  (defadvice git-commit-commit (after delete-window activate)
    (delete-window))

  (defadvice git-commit-abort (after delete-window activate)
    (delete-window))

  (defun magit-quit-session ()
    "Restore the previous window configuration and kill the magit buffer."
    (interactive)
    (kill-buffer)
    (jump-to-register :magit-fullscreen))

  (use-package magit-gh-pulls
    :ensure t
    :config
    (add-hook 'magit-mode-hook 'turn-on-magit-gh-pulls)))



;;; -----js2-mode-----
(use-package js2-mode
  :ensure t
  :defer t
  :mode ("\\.js$" . js2-mode)
  :config

  (setq-default js2-global-externs '("__dirname"
                                     "arguments"
                                     "assert"
                                     "clearInterval"
                                     "clearTimeout"
                                     "console"
                                     "exports"
                                     "location"
                                     "module"
                                     "require"
                                     "setTimeout"
                                     "setInterval"
                                     "JSON"))

  (defun my/js2-indent-function ()
    (interactive)
    (save-restriction
      (widen)
      (let* ((inhibit-point-motion-hooks t)
             (parse-status (save-excursion (syntax-ppss (point-at-bol))))
             (offset (- (current-column) (current-indentation)))
             (indentation (js--proper-indentation parse-status))
             node)

        (save-excursion

          (back-to-indentation)
          ;; consecutive declarations in a var statement are nice if
          ;; properly aligned, i.e:
          ;;
          ;; var foo = "bar",
          ;;     bar = "foo";
          (setq node (js2-node-at-point))
          (when (and node
                     (= js2-NAME (js2-node-type node))
                     (= js2-VAR (js2-node-type (js2-node-parent node))))
            (setq indentation (4 indentation))))

        (indent-line-to indentation)
        (when (> offset 0) (forward-char offset)))))

  (defun my/indent-sexp ()
    (interactive)
    (save-restriction
      (save-excursion
        (widen)
        (let* ((inhibit-point-motion-hooks t)
               (parse-status (syntax-ppss (point)))
               (beg (nth 1 parse-status))
               (end-marker (make-marker))
               (end (progn (goto-char beg) (forward-list) (point)))
               (ovl (make-overlay beg end)))
          (set-marker end-marker end)
          (overlay-put ovl 'face 'highlight)
          (goto-char beg)
          (while (< (point) (marker-position end-marker))
            ;; don't reindent blank lines so we don't set the "buffer
            ;; modified" property for nothing
            (beginning-of-line)
            (unless (looking-at "\\s-*$")
              (indent-according-to-mode))
            (Forward-line))
          (run-with-timer 0.5 nil '(lambda(ovl)
                                     (delete-overlay ovl)) ovl)))))

  (defun my/js2-mode-hook ()
    ;(use-package js)
    (setq ;js-indent-level 4
          indent-tabs-mode nil
          c-basic-offset 2)
    (c-toggle-auto-state 0)
    (c-toggle-hungry-state 1)
    (set (make-local-variable 'indent-line-function) 'my/js2-indent-function)
                                        ;(global-unset-key "S-<backspace>")
    (define-key js2-mode-map [(shift backspace)] 'c-electric-backspace))
                                        ; (define-key js2-mode-map [(meta control |)] 'cperl-lineup)
                                        ; (define-key js2-mode-map [(return)] 'newline-and-indent)
  (add-hook 'js2-mode-hook 'my/js2-mode-hook)

  (use-package hl-todo-mode
    :defer t
    :config
    (add-hook 'js2-mode-hook 'hl-todo-mode))

  (defun js2-docs ()
    (interactive)
    (setq-local helm-dash-docsets '("Lo-Dash"
                                    "NodeJS"
                                    "React"
                                    "Sinon"
                                    "Javascript")))
  (add-hook 'js2-mode-hook 'js2-docs)

  ;; (flycheck-define-checker javascript-flow
  ;;   "A JavaScript syntax and style checker using Flow.
  ;;   See URL `http://flowtype.org/'."
  ;;   :command ("flow" "check" "--old-output-format" source-original)
  ;;   :error-patterns
  ;;   ((error line-start
  ;;           (file-name)
  ;;           ":"
  ;;           line
  ;;           ":"
  ;;           (minimal-match (one-or-more not-newline))
  ;;           ": "
  ;;           (message (minimal-match (and (one-or-more anything) "\n")))
  ;;           line-end))
  ;;   :modes (js-mode js2-mode js3-mode))

  ;; (flycheck-add-next-checker 'javascript-eslint 'javascript-flow)

  (setq js2-mode-show-strict-warnings nil)
  (setq js2-show-parse-errors nil)
  (setq js2-strict-missing-semi-warning nil)
  (js2-mode-hide-warnings-and-errors)

  (rename-modeline "js2-mode" js2-mode "JS2"))


;; -----js2-refactor-----
(use-package js2-refactor
  :ensure t
  :defer t
  :diminish js2-refactor-mode
  :init
  (add-hook 'js2-mode-hook #'js2-refactor-mode))


;;; -----expand-region-----
(use-package expand-region
  :ensure t
  :defer t
  :bind
  (("C-]" . er/expand-region)
   ("C-}" . er/contract-region))
  :config
  (setq er--show-expansion-message t)
  (setq expand-region-fast-keys-enabled nil))


;;; -----yaml-mode-----
(add-to-list 'auto-mode-alist '("\\.sls$" . yaml-mode))


;;; -----fold-this-----
(use-package fold-this
  :ensure t
  :disabled t
  :defer t
  :bind
  (("M-]" . fold-this-all)
   ("M-}" . fold-this-unfold-all)))


;;; -----saveplace-----
(use-package saveplace
  :ensure t
  :defer t
  :init
  (setq-default save-place t)
  :config
  (setq save-place-file
        (expand-file-name ".places" user-emacs-directory)))


;;; -----savehist-----
(use-package savehist
  :ensure t
  :defer t
  :init
  (savehist-mode 1)
  :config
  (setq history-length 1000))


(load "server")
(unless (server-running-p)
  (add-hook 'after-init-hook 'server-start t))


(provide 'init)
;;; init.el ends here
